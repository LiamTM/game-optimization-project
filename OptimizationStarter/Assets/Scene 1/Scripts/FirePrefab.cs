﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Camera ) )]
public class FirePrefab : MonoBehaviour
{
    public GameObject Prefab;
    public float FireSpeed = 5;
    public float numberOfSpheres;

    void Update()
    {
        //Instantiates a Prefab at the mouse position when the "Fire1" Input Button is pressed. 
        //The instantiated Prefab's rigidbody's velocity is then set to Push the prefab away from the center of the camera at a speed of FireSpeed(5 in this case).
        if ( Input.GetButton( "Fire1" ) )
        {
            Vector3 clickPoint = GetComponent<Camera>().ScreenToWorldPoint( Input.mousePosition + Vector3.forward );
            Vector3 FireDirection = clickPoint - this.transform.position;
            FireDirection.Normalize();
            GameObject prefabInstance = GameObject.Instantiate( Prefab, this.transform.position, Quaternion.identity, null );
            prefabInstance.GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;
            numberOfSpheres++;
        }
    }
}
