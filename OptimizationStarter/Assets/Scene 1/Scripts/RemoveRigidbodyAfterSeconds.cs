﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveRigidbodyAfterSeconds : MonoBehaviour {

    Rigidbody m_Rigidbody;
    public float rigidbodyLifetime = 5;
    float timer = 0;

	// Use this for initialization
	void Awake () {
        m_Rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		if (timer < rigidbodyLifetime)
        {
            timer += Time.deltaTime;
        }
        else
        {
            Destroy(m_Rigidbody);
        }
	}
}
