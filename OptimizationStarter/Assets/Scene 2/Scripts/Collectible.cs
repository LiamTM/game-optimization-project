﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public float Radius;


    //Gets an array of every collider within a radius of the collectible. 
    //The array of colliders is then looped through and each collider is checked to to see if the tag is "Player".
    //If there is a match then the collectible is destroyed.
    public void Update()
    {
        Collider[] collidingColliders = Physics.OverlapSphere( this.transform.position, Radius );
        for ( int colliderIndex = 0; colliderIndex < collidingColliders.Length; ++colliderIndex )
        {
            if ( collidingColliders[colliderIndex].tag == "Player" )
            {
                Destroy( this.gameObject );
            }
        }
    }
}
