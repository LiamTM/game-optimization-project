﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCollectible : MonoBehaviour
{
    public GameObject Collectible;

    private GameObject m_currentCollectible;

    // The object that this script is on has multiple empty game object children that serve as the spawn point for the collectibles.
    // If the currentCollectible is equal to null (wich would happen when the collectible is collected and destroyed) then a new Collectible is instantiated at a randomly selected spawn point.
    void Update()
    {
        if ( m_currentCollectible == null )
        {
            m_currentCollectible = Instantiate( Collectible, this.transform.GetChild( UnityEngine.Random.Range( 0, this.transform.GetChildCount() ) ).position, Collectible.transform.rotation );
        }
    }
}
