﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleOptimized : MonoBehaviour
{
    public SpawnCollectibleOptimized m_SpawnCollectibleOptimized;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            m_SpawnCollectibleOptimized.moveCollectible(this.gameObject);
        }
    }
}
