﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{

	void Update ()
    {

        //Gets the camera (Every frame, this should be fixed). Every frames a Raycast is fired from the mouse position with a layer mask filtering out everything but the ground layer.
        //The navmesh component is then gotten and the destination is set to the raycast hit point.

        Camera camera = GameObject.Find( "Main Camera" ).GetComponent<Camera>();
        RaycastHit hit = new RaycastHit();
        if ( Physics.Raycast( camera.ScreenPointToRay( Input.mousePosition ), out hit, float.MaxValue, LayerMask.GetMask( "Ground" ) ) )
        {
            GetComponent<NavMeshAgent>().destination = hit.point;
        }
	}
}
