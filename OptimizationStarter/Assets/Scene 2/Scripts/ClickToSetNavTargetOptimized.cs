﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTargetOptimized : MonoBehaviour
{
    Camera m_Camera;
    NavMeshAgent m_NavMeshAgent;

    void Start()
    {
       {
            m_Camera = Camera.main;
            m_NavMeshAgent = GetComponent<NavMeshAgent>();
        }
    }

    void Update ()
    {
        if (Input.GetButton("Fire1"))
        {
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(m_Camera.ScreenPointToRay(Input.mousePosition), out hit, float.MaxValue, LayerMask.GetMask("Ground")))
            {
                m_NavMeshAgent.destination = hit.point;
            }
        }
	}
}
