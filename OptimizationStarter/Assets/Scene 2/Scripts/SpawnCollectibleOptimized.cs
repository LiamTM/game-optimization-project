﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCollectibleOptimized : MonoBehaviour
{
    public void moveCollectible(GameObject Collectible)
    {
        Vector3 newPosition = this.transform.GetChild(UnityEngine.Random.Range(0, this.transform.childCount)).position;

        Collectible.transform.position = newPosition;

    }
}
